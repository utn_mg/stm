# Sistemas de Transmisión y Microondas

1. [Introducción](1.%20Introduccion.md)
2. [Sistemas de Comunicaciones alámbricos e inalámbricos](2.%20Sistemas%20de%20Comunicaciones%20al%C3%A1mbricos%20e%20inal%C3%A1mbricos.md)
3. [Ondas Electromagnéticas Transversales](3.%20Ondas%20Electromagn%C3%A9ticas%20Transversales)
4. [Tipos de líneas de transmisión](4.%20Tipos%20de%20l%C3%ADneas%20de%20Transmisi%C3%B3n.md)
5. [Evolución, frecuencias y aplicaciones](5.%20Evoluci%C3%B3n%2C%20frecuencias%20y%20aplicaciones.md)
6. El modo TEM y el análisis de líneas por voltajes y corrientes
7. El modo dominante, los modos superiores y el análisis por campos electromagnéticos
8. Cicuito equivalente de una línea de transmisión
9. Propagación de onda en línea de transmisión
10. Pérdidas en líneas de transmisión
11. Diagrama de Smith
12. Ondas incidentes y reflejadas
13. Ondas estacionarias
14. Impedancia de entrada de una línea de transmisión
15. Reflectometría en el dominio del tiempo
16. Líneas de transmisión de microcinta y cinta
    - Aplicaciones
    - La línea de cinta
    - La microcinta

17. Teoría de las líneas de dos conductores
    - Ecuación general de una línea de transmisión
    - Propagación en líneas acopladas
    - Impedancia de entrada de una línea terminada con una carga arbitraria
    - Impedancia de entrada de una línea terminada en corto circuito
    - Impedancia de entrada de una línea terminada en circuito abierto
    - Obtención de Z0 y γ a partir de las impedancias de entrada medidas en líneas terminadas en corto circuito y circuito abierto
    - Reactancia de entrada y aplicaciones de líneas sin pérdidas terminadas en corto circuito y en circuito abierto
    - Líneas desacopladas y ondas estacionarias
    - Reflexiones en el generador
    - La matriz de transmisión
    - Pérdidas en una línea y eficiencia de transmisión de potencia
    - Uso de la carta de Smith para líneas con pérdidas
    - Acoplamiento de impedancias
    - Resonancia, factor de calidad y cavidades resonantes
18. Aplicaciones de las líneas de dos conductores
    - El cable bifilar
    - El cable multipar trenzado
    - El cable coaxial terrestre
    - El cable coaxial submarino
19. Teoria de las guías de ondas
    - La onda electromagnética plana
    - La onda plana en un ambiente sin pérdidas
    - La onda plana en un medio con pérdidas
    - Teoría general de los modos TE
    - Teoría general de los modos TM
    - La guía de dos placas paralelas
    - Impedancias de los modos TE y TM. Teoría general
    - Guías rectangulares
    - Guías circulares
20. Algunos componentes fundamentales de los sistemas de guías de ondas
    - Comparación entre una línea coaxial y una guía de ondas
    - Ondas estacionarias en guías de onda
    - La matriz de dispersión
    - Acoplamiento de impedancias
    - Algunos otros componentes y dispositivos de microondas
21. Teoría de las fibras ópticas
    - Introducción
    - Tipos de fibras y cables ópticos
    - Propagación y ecuación característica de una fibra de índice escalonado
    - Método aproximado para resolver la ecuación característica de una fibra de índice escalonado
    - Constante de fase y frecuencia de corte de los modos en una fibra de índice escalonado
    - Conceptos adicionales sobre el modo dominante
    - Modos linealmente polarizados
    - La fibra monomodo
    - Teoría de la óptica radial o geométrica
    - Propagación en fibras multimodo de índice escalonado
    - Ángulo de aceptación y apertura numérica
    - Propagación en fibras multimodo de índice gradual
22. Componentes y sistemas con fibras ópticas
    - Introducción
    - Atenuación
    - Producto ancho de banda x distancia
    - Fuentes de luz
    - Amplificadores
    - Detectores ópticos
    - Ejemplos de sistemas terrestres y submarinos

Referencias:

- Rodolfo Neri Vela (2013), Líneas de Transmisión, Primera Edición, Universidad Veracruzana
- Wayne Tomasi (2001), Sistemas de Comunicaciones Electrónicas, Cuarta Edición, Prentice Hall. 